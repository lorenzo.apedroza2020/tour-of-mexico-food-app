package com.mexicanfoodapp.lorenzo.tourofmexicofoodapp;

import android.support.annotation.DrawableRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    ImageView foodImage;
    Button changeFoodImageButton;

    @DrawableRes int[] mexicanFood;
    @DrawableRes int[] alreadyShownMexicanFood;

    int randomPictureIndex;
    int newImagesInArray;
    boolean alreadyShown;

    Random randomizer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        foodImage = findViewById(R.id.foodImageID);
        changeFoodImageButton = findViewById(R.id.changeFoodPicButtonID);

        mexicanFood = new int[] {R.drawable.albondigas_soup, R.drawable.arroz_con_leche_de_coco,
                R.drawable.breakfast_chilaquiles, R.drawable.mexican_torta, R.drawable.taco, R.drawable.tamales,
                R.drawable.the_perfect_flan, R.drawable.white_pozole};

        alreadyShownMexicanFood = new int[mexicanFood.length+1];

        newImagesInArray = 0;
        alreadyShown = false;
        randomizer = new Random();

        changeFoodImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                do{
                    randomPictureIndex = randomizer.nextInt(mexicanFood.length);
                    for (@DrawableRes int alreadyShownImageID: alreadyShownMexicanFood)
                    {
                        if (alreadyShownImageID == mexicanFood[randomPictureIndex]){
                            {
                                alreadyShown = true;
                                break;  //get out of for loop to get another random #
                            }

                        }
                        else
                            alreadyShown = false;
                    }
                }while (alreadyShown);
                newImagesInArray++;
                if(newImagesInArray == alreadyShownMexicanFood.length+1)
                {
                    alreadyShownMexicanFood = new int[mexicanFood.length+1]; //clear out the old array again
                    newImagesInArray = 0;
                }

                foodImage.setImageResource(mexicanFood[randomPictureIndex]);
            }
        });


    }
}
